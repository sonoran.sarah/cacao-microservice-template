package adapters

import (
	"context"
	"fmt"
	"log"

	"gitlab.com/cyverse/cacao-microservice-template/types"
	"gitlab.com/cyverse/cacao-microservice-template/utils"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

//MongoAdapter ... Implementation of the persistent storage port
type MongoAdapter struct {
	mclient     mongo.Client
	mcollection mongo.Collection
}

//Init ... initialize mongodb adapter
func (c *MongoAdapter) Init(config utils.Config) {

	println("init mongodb adapter")

	//Probably get host from CONFIG object
	clientOptions := options.Client().ApplyURI("mongodb://localhost:27017")
	// Connect to MongoDB
	client, err := mongo.Connect(context.TODO(), clientOptions)
	c.mclient = *client

	if err != nil {
		log.Fatal(err)
	}

	// Check the connection
	err = client.Ping(context.TODO(), nil)

	if err != nil {
		log.Fatal(err)
	}

	//	Likely get collection and database from CONFIG object
	collection := client.Database("test").Collection("myTestData")
	c.mcollection = *collection

	fmt.Println("Connected to MongoDB!")
}

func (c MongoAdapter) Fetch() []*types.MyDomainData {
	// Pass these options to the Find method
	findOptions := options.Find()

	// Here's an array in which you can store the decoded documents
	var results []*types.MyDomainData

	// Passing bson.D{{}} as the filter matches all documents in the collection
	cur, err := c.mcollection.Find(context.TODO(), bson.D{{}}, findOptions)
	if err != nil {
		log.Fatal(err)
	}

	// Finding multiple documents returns a cursor
	// Iterating through the cursor allows us to decode documents one at a time
	for cur.Next(context.TODO()) {

		// create a value into which the single document can be decoded
		var elem types.MyDomainData
		err := cur.Decode(&elem)
		if err != nil {
			log.Fatal(err)
		}

		results = append(results, &elem)
	}

	if err := cur.Err(); err != nil {
		log.Fatal(err)
	}

	// Close the cursor once finished
	cur.Close(context.TODO())

	fmt.Printf("Found multiple documents (array of pointers): %+v\n", results)
	return results
}

func (c MongoAdapter) Create(d types.MyDomainData) error {

	insertResult, err := c.mcollection.InsertOne(context.TODO(), d)
	if err != nil {
		log.Fatal(err)
		return err
	}

	fmt.Println("Inserted a single document: ", insertResult.InsertedID)

	return nil

}

func (c MongoAdapter) Read(d types.MyDomainData) error {

	var result types.MyDomainData

	err := c.mcollection.FindOne(context.TODO(), d).Decode(&result)
	if err != nil {
		log.Fatal(err)
		return err
	}

	fmt.Printf("Found a single document: %+v\n", result)
	return nil
}

func (c MongoAdapter) Update(d types.MyDomainData, e types.MyDomainData) error {

	updateResult, err := c.mcollection.UpdateOne(context.TODO(), d, e)
	if err != nil {
		log.Fatal(err)
		return err
	}

	fmt.Printf("Matched %v documents and updated %v documents.\n", updateResult.MatchedCount, updateResult.ModifiedCount)
	return nil
}

func (c MongoAdapter) Delete(d types.MyDomainData) error {

	deleteResult, err := c.mcollection.DeleteMany(context.TODO(), d)
	if err != nil {
		log.Fatal(err)
		return err
	}
	fmt.Printf("Deleted %v documents in the collection\n", deleteResult.DeletedCount)
	return nil

}
